using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Level2
{
    public class DoorScript : MonoBehaviour
    {
        [SerializeField] private List<GameObject> toHideOnClick;
        [SerializeField] private GameObject newCollisionFloor;

        public void Hide()
        {
            foreach (var item in toHideOnClick)
            {
                item.SetActive(false);
            }

            newCollisionFloor.SetActive(true);
        }
    }
}
