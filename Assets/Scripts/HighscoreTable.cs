﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighscoreTable : MonoBehaviour {

    private Transform entryContainer;
    private Transform entryTemplate;
    private List<Transform> highscoreEntryTransformList;

    private int currentRank = 0;
    
    private void Awake() {
        entryContainer = transform.Find("highscoreEntryContainer");
        entryTemplate = entryContainer.Find("highscoreEntryTemplate");

        entryTemplate.gameObject.SetActive(false);
        string jsonString = PlayerPrefs.GetString("highscoreTable");
        Highscores highscores = JsonUtility.FromJson<Highscores>(jsonString);

        //pour tester a effacer
        if (highscores == null) {
            // There's no stored table, initialize
            Debug.Log("Initializing table with default values...");
            AddHighscoreEntry(1000000, "TST");
            AddHighscoreEntry(2323, "BOS");
            AddHighscoreEntry(11, "ALS");
            AddHighscoreEntry(22, "POL");
            AddHighscoreEntry(33333, "MLP");
            AddHighscoreEntry(22222, "CSD");
            AddHighscoreEntry(9999, "ZSZ");
            AddHighscoreEntry(4311, "TST");
            AddHighscoreEntry(3333, "ZZZ");
            AddHighscoreEntry(131245, "XYZ");

            // Reload
            jsonString = PlayerPrefs.GetString("highscoreTable");
            highscores = JsonUtility.FromJson<Highscores>(jsonString);
        }

        get10Entries(0);
    }

    private void CreateHighscoreEntryTransform(HighscoreEntry highscoreEntry, Transform container, List<Transform> transformList, int startRank) {
        float templateHeight = 80f;
        Transform entryTransform = Instantiate(entryTemplate, container);
        RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform>();
        entryRectTransform.anchoredPosition = new Vector2(0, -templateHeight * transformList.Count);
        entryTransform.gameObject.SetActive(true);

        int rank = startRank + transformList.Count + 1;
        string rankString;
        switch (rank) {
        default:
            rankString = rank + "e"; break;

        case 1: rankString = "1er"; break;
        }

        entryTransform.Find("posText").GetComponent<Text>().text = rankString;

        float score = highscoreEntry.score;

        entryTransform.Find("scoreText").GetComponent<Text>().text = score.ToString("0.00");

        string name = highscoreEntry.name;
        entryTransform.Find("nameText").GetComponent<Text>().text = name;

        // Set background visible odds and evens, easier to read
        entryTransform.Find("background").gameObject.SetActive(rank % 2 == 1);
        
        // Highlight First
        if (rank == 1) {
            entryTransform.Find("posText").GetComponent<Text>().color = Color.yellow;
            entryTransform.Find("scoreText").GetComponent<Text>().color = Color.yellow;
            entryTransform.Find("nameText").GetComponent<Text>().color = Color.yellow;
        }

        // Set tropy
        switch (rank) {
        case 1:
            entryTransform.Find("trophy").GetComponent<RawImage>().color = new Color32(255, 215, 0, 255);
            break;
        case 2:
            entryTransform.Find("trophy").GetComponent<RawImage>().color = new Color32(192, 192, 192, 255);
            break;
        case 3:
            entryTransform.Find("trophy").GetComponent<RawImage>().color = new Color32(205, 127, 50, 255);
            break;
        default:
            entryTransform.Find("trophy").gameObject.SetActive(false);
            break;
        }

        transformList.Add(entryTransform);
    }

    private void AddHighscoreEntry(int score, string name) {
        // Create HighscoreEntry
        HighscoreEntry highscoreEntry = new HighscoreEntry { score = score, name = name };
        
        // Load saved Highscores
        string jsonString = PlayerPrefs.GetString("highscoreTable");
        Highscores highscores = JsonUtility.FromJson<Highscores>(jsonString);

        if (highscores == null) {
            // There's no stored table, initialize
            highscores = new Highscores() {
                highscoreEntryList = new List<HighscoreEntry>()
            };
        }

        // Add new entry to Highscores
        highscores.highscoreEntryList.Add(highscoreEntry);

        // Save updated Highscores
        string json = JsonUtility.ToJson(highscores);
        PlayerPrefs.SetString("highscoreTable", json);
        PlayerPrefs.Save();
    }

    public void clear()
    {
        foreach (Transform entry in highscoreEntryTransformList)
        {
            Destroy(entry.gameObject);
        }
    }

    private void get10Entries(int start, int end = 0)
    {
        if(end == 0)
        {
            end = start + 10;
        }
        string jsonString = PlayerPrefs.GetString("highscoreTable");
        Highscores highscores = JsonUtility.FromJson<Highscores>(jsonString);
        // Sort entry list by Score
        for (int i = 0; i < highscores.highscoreEntryList.Count; i++)
        {
            for (int j = i + 1; j < highscores.highscoreEntryList.Count; j++)
            {
                if (highscores.highscoreEntryList[i].score > highscores.highscoreEntryList[j].score)
                {
                    // Swap
                    HighscoreEntry tmp = highscores.highscoreEntryList[i];
                    highscores.highscoreEntryList[i] = highscores.highscoreEntryList[j];
                    highscores.highscoreEntryList[j] = tmp;
                }
            }
        }

        highscoreEntryTransformList = new List<Transform>();
        //
        for (int i = start; i < end; i++)
        {
            CreateHighscoreEntryTransform(highscores.highscoreEntryList[i], entryContainer, highscoreEntryTransformList, start);
        }

        currentRank = start;
    }

    public void first()
    {
        clear();
        get10Entries(0);
    }

    public void previous()
    {
        if (currentRank == 0) return;

        clear();
        get10Entries(currentRank - 10);
    }

    public void next()
    {
        string jsonString = PlayerPrefs.GetString("highscoreTable");
        Highscores highscores = JsonUtility.FromJson<Highscores>(jsonString);

        if (currentRank + 10 >= highscores.highscoreEntryList.Count) return;

        clear();
        int endLastNumber = highscores.highscoreEntryList.Count;

        if(highscores.highscoreEntryList.Count - 20 > currentRank)
        {
            endLastNumber = currentRank + 20;
        }
        get10Entries(currentRank + 10, endLastNumber);
    }

    public void last()
    {
        clear();
        string jsonString = PlayerPrefs.GetString("highscoreTable");
        Highscores highscores = JsonUtility.FromJson<Highscores>(jsonString);

        int startLastNumber = highscores.highscoreEntryList.Count - 10;
        if(highscores.highscoreEntryList.Count % 10 > 0)
        {
            startLastNumber = highscores.highscoreEntryList.Count - highscores.highscoreEntryList.Count % 10;
        }
        get10Entries(startLastNumber, highscores.highscoreEntryList.Count);
    }

    private class Highscores {
        public List<HighscoreEntry> highscoreEntryList;
    }
}
