using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestTrigger : AnimationTrigger
{
    [SerializeField] private string chestErrorClip;

    public void startAnimation(bool isError) { 
        switch(isError) {
            case true:
                TheAnimator.Play(ChestErrorClip);
            break;
            case false:
                startAnimation();
            break;
        }
    }
    public string ChestErrorClip { get => chestErrorClip; }
}
