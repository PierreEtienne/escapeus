using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationTrigger : MonoBehaviour
{
    [Header("Animation")]
    [SerializeField] private Animator theAnimator;
    [SerializeField] private string theAnimationClip;

    public void startAnimation() {
        theAnimator.Play(TheAnimationClip);
    }

    public Animator TheAnimator { get => theAnimator; }
    public string TheAnimationClip { get => theAnimationClip; }
}
