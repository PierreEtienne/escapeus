using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeGainScript : MonoBehaviour
{
    [SerializeField] private float timeGain = 0.0f;
    [SerializeField] private float timeLoss = 0.0f;

    public float TimeGain { get => timeGain; set => timeGain = value; }
    public float TimeLoss { get => timeLoss; set => timeLoss = value; }
}
