/*
* Represents a single High score entry
* */
[System.Serializable]
public class HighscoreEntry
{
    public float score;
    public string name;
}