using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameElevatorController : MonoBehaviour
{
    [SerializeField]
    private GameObject gameTrigger;
    [SerializeField]
    private GameObject selector;
    [SerializeField]
    private GameObject panel;
    [SerializeField]
    private List<GameObject> levels;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SceneReady(int sceneIndex) {
        switch (sceneIndex)
        {
            case 0:
                selector.SetActive(true);
                panel.SetActive(true);
                gameTrigger.GetComponent<PlayController>().IsGameSpawned = true;
                break;
            default:
                break;
        }
    }

    public void changeLevel(int newLevel) {
        levels[newLevel - 2].SetActive(false);
        levels[newLevel - 1].SetActive(true);
    }
}
