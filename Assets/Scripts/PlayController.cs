﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

/*
https://www.youtube.com/watch?v=QbhuJwsC22Q : Hide planes
https://www.youtube.com/watch?v=1Edsroa16fo : Hide planes
https://www.youtube.com/watch?v=eI2GOuEMGfQ : Unity Debug
https://www.youtube.com/watch?v=lkQ1GSJUOJk : Object Selection
*/

[RequireComponent(typeof(ARPlaneManager))]
public class PlayController : MonoBehaviour
{
    [SerializeField]
    private GameObject indicator;
    [SerializeField]
    private GameObject indicatorReplacement;
    [SerializeField]
    private GameObject textPanel;
    [SerializeField]
    private GameObject selector;
    [SerializeField]
    private float generateRayAfterSeconds = 2.0f;
    [SerializeField]
    private float rayDistanceFromCamera = 10.0f;
    [SerializeField]
    private Color raycastHitOkColor;
    [SerializeField]
    private Color raycastHitNoColor;
    [SerializeField]
    private GameObject debugMessage;
    [SerializeField]
    private TMPro.TextMeshProUGUI timerText;
    [SerializeField]
    private TMPro.TextMeshProUGUI endTimerText;
    [SerializeField]
    private TMPro.TextMeshProUGUI tip1Text;
    [SerializeField]
    private TMPro.TextMeshProUGUI tip2Text;
    [SerializeField]
    private TMPro.TextMeshProUGUI tip3Text;
    [SerializeField]
    private TMPro.TextMeshProUGUI bonusText;
    [SerializeField]
    private TMPro.TextMeshProUGUI finalTimerText;
    [SerializeField]
    private TMPro.TextMeshProUGUI tipText;
    [SerializeField]
    private GameObject tipYesButton;
    [SerializeField]
    private GameObject tipNoButton;
    [SerializeField]
    private GameObject endPanel;
    [SerializeField]
    private GameObject backButton;
    [SerializeField]
    private GameObject rankingButton;
    [SerializeField]
    private GameObject finalText;
    [SerializeField]
    private GameObject nameInput;
    [SerializeField]
    private GameObject helpMenu;
    [SerializeField]
    private GameObject gameMenu;
    [SerializeField]
    private GameObject tipMenu;
    [SerializeField]
    private GameObject inventory;
    [SerializeField]
    private GameObject theLight;
    [SerializeField]
    private GameObject hideEverythingCanvas;
    [SerializeField]
    private Level3Controller level3Controller;
    [SerializeField]
    private int indiceBonus = 30;

    private ARPlaneManager arPlaneManager;
    private ARSessionOrigin arSessionOrigin;
    private ARRaycastManager arRaycastManager;
    private GameObject placedObject;
    private Pose pose;
    private bool placementPoseIsValid;
    private bool isPlaced;
    private float rayTimer = 0;
    private float startTime;
    private bool finnished = false;
    private float finalTime;
    private bool indice1;
    private bool indice2;
    private bool indice3;
    private float tipTime = 0;
    private int gameState = 0;
    private GameObject placedElevator;
    private int lockedState = 14;
    private bool isGameSpawned;
    private bool doorUnlocked = false;
    private int currentLevel = 1;
    private int currentTipLevel = 0;
    private string currentTipText;
    private bool hasBonus = false;

    public bool IsGameSpawned {
        get { return isGameSpawned; }
        set { isGameSpawned = value; }
    }

    void Awake() 
    {
        arPlaneManager = GetComponent<ARPlaneManager>();
        arSessionOrigin = GetComponent<ARSessionOrigin>();
        arRaycastManager = GetComponent<ARRaycastManager>();

        arPlaneManager.planesChanged += PlaneChanged;
    }

    void Update()
    {
        if (!isPlaced) {
            UpdatePlacement();
            UpdateIndicator();
        }

        RaycastHit[] raycastHits = Physics.RaycastAll(arSessionOrigin.camera.transform.position, Vector3.down, float.PositiveInfinity, 1 << 6);
        bool isOutside = raycastHits.Length == 0;
        hideEverythingCanvas.SetActive(isGameSpawned && isPlaced && isOutside);

        String tag = "None";
        GameObject theGameObject = null;
        if (isOutside) {}
        else if (generateRayAfterSeconds >= rayTimer) {
            Ray ray = arSessionOrigin.camera.ScreenPointToRay(selector.transform.position);
            RaycastHit hitObject;
            Transform theTransform;
            RawImage rawImage = selector.GetComponent<RawImage>();
            Text debugText = debugMessage.GetComponentInChildren<Text>();

            if (Physics.Raycast(ray, out hitObject, rayDistanceFromCamera) && IsRaycastable(hitObject.transform.tag)) {
                theGameObject = hitObject.transform.gameObject;
                theTransform = hitObject.transform;
                theGameObject = theTransform.gameObject;
                debugText.text = String.Format("hit: {0} : {1} - {2}", theTransform.tag, theTransform.name, lockedState);
                tag = theTransform.tag;
                rawImage.color = raycastHitOkColor;
            } else {
                debugText.text = String.Format("waiting for hit {0}", lockedState);

                rawImage.color = raycastHitNoColor;
            }

            rayTimer = 0;
        } else {
            rayTimer += Time.deltaTime * 1.0f;
        }

        if (isOutside) {}
        else if (IsScreenTouched() && tag == "u-key")
        {
            if (gameState == 0)
            {
                inventory.GetComponent<InventoryScript>().AddObject("u-key");
                gameState = 1;
                theGameObject.GetComponent<LightTrigger>().startAnimation(false);
                Destroy(theGameObject);
            }
            else
            {
                theGameObject.GetComponent<LightTrigger>().startAnimation(true);
            }
            //theGameObject.GetComponent<LightTrigger>().startAnimation(!(gameState == 0));
        }
        else if (IsScreenTouched() && tag == "u-Door")
        {
            if (lockedState == 0) {
                theGameObject.GetComponent<Level2.DoorScript>().Hide();
                /*Finnish();
                endPanel.SetActive(true);
                placedElevator.GetComponent<GameElevatorController>().changeLevel(3);*/
            }
            theGameObject.GetComponent<LightTrigger>().startAnimation(lockedState > 0);
        }
        else if (IsScreenTouched() && tag == "u-Book")
        {
            theGameObject.GetComponent<AnimationTrigger>().startAnimation();
        }
        else if (IsScreenTouched() && tag == "u-Chest" && gameState < 2)
        {
            if (gameState == 1) {
                gameState = 2;
                theGameObject.GetComponent<ChestTrigger>().startAnimation(false);
                inventory.GetComponent<InventoryScript>().RemoveObject("u-key");
            } else {
                theGameObject.GetComponent<ChestTrigger>().startAnimation(true);
            }
        }
        else if (IsScreenTouched() && tag == "u-Ladder")
        {
            placedElevator.GetComponent<GameElevatorController>().changeLevel(2);
            currentLevel = 2;
        }
        else if (IsScreenTouched() && tag == "u-Locker") {
            LockerTrigger locker = theGameObject.GetComponent<LockerTrigger>();
            if (locker.IsAnimatable) {
                locker.StartTrigger();
            }
        }
        else if (IsScreenTouched() && tag == "u-Door2")
        {
            if (doorUnlocked)
            {
                Finnish();
                endPanel.SetActive(true);
                theGameObject.GetComponent<LightTrigger>().startAnimation(false);

                float timeGain = inventory.GetComponent<InventoryScript>().GetTimeGain();
                string bonusMinutes = ((int)timeGain / 60).ToString();
                string bonusSeconds = (timeGain % 60).ToString("00");

                string minutes = ((int)finalTime / 60).ToString();
                string seconds = (finalTime % 60).ToString("00.00");

                endTimerText.text = minutes + ":" + seconds;
                timerText.text = minutes + ":" + seconds;
                tip1Text.text = indice1 ? indiceBonus.ToString("+0:00") : "+0:00";
                tip2Text.text = indice2 ? indiceBonus.ToString("+0:00") : "+0:00";
                tip3Text.text = indice3 ? indiceBonus.ToString("+0:00") : "+0:00";
                bonusText.text = hasBonus ? string.Format("-{0}:{1}", bonusMinutes, bonusSeconds) : "-0:00";

                float finalTimerWithChanges = finalTime + tipTime;
                if (hasBonus) finalTimerWithChanges -= timeGain;
                string minutesFinal = ((int)finalTimerWithChanges / 60).ToString();
                string secondsFinal = (finalTimerWithChanges % 60).ToString("00.00");

                finalTimerText.text = minutesFinal + ":" + secondsFinal;
                finalTime = finalTimerWithChanges;
            } else {
                theGameObject.GetComponent<LightTrigger>().startAnimation(true);
            }

        }
        else if (IsScreenTouched() && tag == "u-Button")
        {
            theGameObject.GetComponent<Animator>().SetTrigger("ButtonPress");
        }
        else if (IsScreenTouched() && tag == "u-Ladder2")
        {
            placedElevator.GetComponent<GameElevatorController>().changeLevel(3);
            currentLevel = 3;
        }
        else if (IsScreenTouched() && tag == "u-Gold0")
        {
            inventory.GetComponent<InventoryScript>().AddObject("u-gold");
            Destroy(theGameObject.transform.parent.gameObject);
        }

        if (placementPoseIsValid && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began && !isPlaced) {
            isPlaced = true;
            indicator.SetActive(false);

            PlaceReplacement();
            HideMessage();
            TurnOffPlaneDetection();
            startTime = Time.time;

        }
        else if (isPlaced)
        {
            UpdateTimer();
        }
    }

    public void onDoorOpen ()  {
        doorUnlocked = true;
    } 

    private bool IsScreenTouched() => Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began;

    private bool IsRaycastable(string tag)
    {
        switch (tag)
        {
            case "u-Floor":
            case "u-Roof":
            case "u-Wall":
                return false;
            default:
                return true;
        }
    }

    private void UpdateTimer()
    {
        if (finnished)
            return;

        float t = Time.time - startTime;
        string minutes = ((int)t / 60).ToString();
        string seconds = (t % 60).ToString("00.00");
        
        timerText.text = minutes + ":" + seconds;
    }

    private void HideMessage()
    {
        textPanel.SetActive(false);
    }

    private void TurnOffPlaneDetection()
    {
        foreach (var plane in arPlaneManager.trackables)
        {
            plane.gameObject.SetActive(false);
        }
        
        arPlaneManager.enabled = false;
    }

    private void UpdatePlacement() {
        var screenCenter = arSessionOrigin.camera.ViewportToScreenPoint(new Vector3(0.5f,0.5f));
        var hits = new List<ARRaycastHit>();
        arRaycastManager.Raycast(screenCenter, hits, TrackableType.Planes);
        placementPoseIsValid = hits.Count > 0;
        if (placementPoseIsValid) {
            pose = hits[0].pose;

            var camForward = arSessionOrigin.camera.transform.forward;
            var camBearing = new Vector3(camForward.x,0,camForward.z).normalized;
            pose.rotation = Quaternion.LookRotation(camBearing);
        }
    }

    private void UpdateIndicator() {
        if (placementPoseIsValid && !isPlaced) {
            indicator.SetActive(true);
            indicator.transform.SetPositionAndRotation(pose.position, pose.rotation);
        } else {
            indicator.SetActive(false);
        }
    }

    private void PlaneChanged(ARPlanesChangedEventArgs args)
    {
        if(args.added != null && placedObject == null && !isPlaced)
        {
            ARPlane arPlane = args.added[0];
            placedObject = Instantiate(indicator, arPlane.transform.position, Quaternion.identity);
        }
    }

    private void PlaceReplacement() {
        Pose replacementPose = Pose.identity;
        replacementPose.position = indicator.transform.position;
        replacementPose.rotation = indicator.transform.rotation;

        GameObject newObject = Instantiate(indicatorReplacement, replacementPose.position, replacementPose.rotation);
        newObject.SetActive(true);
        newObject.GetComponent<Animator>().SetTrigger("BringLevel1Trigger");
        theLight.SetActive(false);

        placedElevator = newObject;
    }

    private void Finnish()
    {
        finnished = true;
        finalTime = Time.time - startTime;
        timerText.color = Color.yellow;
    }

    private void AddHighscoreEntry(float score, string name)
    {
        // Create HighscoreEntry
        HighscoreEntry highscoreEntry = new HighscoreEntry { score = score, name = name };

        // Load saved Highscores
        string jsonString = PlayerPrefs.GetString("highscoreTable");
        Highscores highscores = JsonUtility.FromJson<Highscores>(jsonString);

        if (highscores == null)
        {
            // There's no stored table, initialize
            highscores = new Highscores()
            {
                highscoreEntryList = new List<HighscoreEntry>()
            };
        }

        // Add new entry to Highscores
        highscores.highscoreEntryList.Add(highscoreEntry);

        // Save updated Highscores
        string json = JsonUtility.ToJson(highscores);
        PlayerPrefs.SetString("highscoreTable", json);
        PlayerPrefs.Save();
    }

    public void AddScore(TMPro.TextMeshProUGUI theText) {
        string leText = theText.text.ToUpper();
        Debug.Log($"leText ({leText}) Length {leText.Length}");
        if (leText.Length != 4) return; 

        backButton.SetActive(true);
        rankingButton.SetActive(true);
        finalText.GetComponent<TMPro.TextMeshProUGUI>().text = $"Bravo {leText} pour avoir terminé le niveau !";
        nameInput.SetActive(false);
        AddHighscoreEntry(finalTime, leText);
    }

    public void ShowMenu() {
        if (tipMenu.activeSelf) return;
        if (helpMenu.activeSelf) return;
        gameMenu.SetActive(true);
    }

    public void ShowTipMenu()
    {
        if (gameMenu.activeSelf || helpMenu.activeSelf) return;
        if (tipMenu.activeSelf)
        {
            tipMenu.SetActive(false);
            return;
        }
        if (currentLevel == currentTipLevel)
        {
            tipText.text = currentTipText;
            tipYesButton.SetActive(false);
            tipNoButton.SetActive(false);
        }
        else
        {
            currentTipText = "Voulez-vous payer 30 secondes pour recevoir une indice ?";
            tipText.text = currentTipText;
            tipYesButton.SetActive(true);
            tipNoButton.SetActive(true);
        }
        tipMenu.SetActive(true);
    }

    public void TextTipMenu()
    {
        switch (currentLevel)
        {
            case 1:
                currentTipText = "Regardez bien la première table en entrant. Le poème contient certains caractères spéciaux...";
                tipText.text = currentTipText;
                indice1 = true;
                currentTipLevel = 1;
                break;
            case 2:
                currentTipText = "Les peintures ont un lien avec le code. Le nombre d'apparitions du symbole pourrait avoir un lien avec l'ordre...";
                tipText.text = currentTipText;
                indice2 = true;
                currentTipLevel = 2;
                break;
            case 3:
                currentTipText = "Les quatre feux doivent s'allumer en vert. Ne pensez pas trop dur, peut-être que la combinaison est tout simplement aléatoire...";
                tipText.text = currentTipText;
                indice3 = true;
                currentTipLevel = 3;
                break;
        }
        tipTime += indiceBonus;

        tipYesButton.SetActive(false);
        tipNoButton.SetActive(false);
    }

    public void HandeCubeVisibilityUnlock(int lockerIndex) {
        lockedState ^= 1 << lockerIndex;
    }

    public void HandeCubeVisibilityLock(int lockerIndex) {
        lockedState |= 1 << lockerIndex;
    }

    public void AddCoinInventory() {
        hasBonus = true;
        inventory.GetComponent<InventoryScript>().AddObject("u-gold");
    }

    private class Highscores
    {
        public List<HighscoreEntry> highscoreEntryList;
    }
}
