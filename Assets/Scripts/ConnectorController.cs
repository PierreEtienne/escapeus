using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectorController : MonoBehaviour
{
    [SerializeField] private PlayController playConnector;

    public void ChestOpenInvoke() {
        playConnector.AddCoinInventory();
    }
}
