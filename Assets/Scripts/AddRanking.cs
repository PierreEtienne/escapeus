/*using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddRanking : MonoBehaviour
{
    [SerializeField]
    private GameObject backButton;
    [SerializeField]
    private GameObject rankingButton;
    [SerializeField]
    private GameObject input;
    [SerializeField]
    private GameObject text;

    private void AddHighscoreEntry(float score, string name)
    {
        // Create HighscoreEntry
        HighscoreEntry highscoreEntry = new HighscoreEntry { score = score, name = name };

        // Load saved Highscores
        string jsonString = PlayerPrefs.GetString("highscoreTable");
        Highscores highscores = JsonUtility.FromJson<Highscores>(jsonString);

        if (highscores == null)
        {
            // There's no stored table, initialize
            highscores = new Highscores()
            {
                highscoreEntryList = new List<HighscoreEntry>()
            };
        }

        // Add new entry to Highscores
        highscores.highscoreEntryList.Add(highscoreEntry);

        // Save updated Highscores
        string json = JsonUtility.ToJson(highscores);
        PlayerPrefs.SetString("highscoreTable", json);
        PlayerPrefs.Save();
    }

    private class Highscores
    {
        public List<HighscoreEntry> highscoreEntryList;
    }

    public void Add(TMPro.TextMeshProUGUI theText) {
        string leText = theText.text.ToUpper();
        Debug.Log($"leText ({leText}) Length {leText.Length}");
        if (leText.Length != 4) return; 

        backButton.SetActive(true);
        rankingButton.SetActive(true);
        text.GetComponent<TMPro.TextMeshProUGUI>().text = $"Bon matin {leText}";
        input.SetActive(false);
    }
}*/