using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockerTrigger : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private string triggerName;
    [SerializeField] private bool animatable;

    public bool IsAnimatable { get { return animatable; } set { animatable = value; }}

    public void StartTrigger() {
        if (IsAnimatable) {
            animator.SetTrigger(triggerName);
        }
    }
}
