using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class GameManager : MonoBehaviour
{
    public bool GamePaused;

    public void PauseGame()
    {
        Time.timeScale = 0;
        GamePaused = true;

    }
    // Start is called before the first frame update
    void Start()
    {
        GamePaused = false;
    }

    // Update is called once per frame
    void Update()
    {

    }
}