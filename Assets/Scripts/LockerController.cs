using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockerController : MonoBehaviour
{
    [SerializeField] private List<GameObject> screenList;
    [SerializeField] private int unlockedIndex;
    [SerializeField] private int lockerIndex;

    public List<GameObject> ScreenList { get { return screenList; }}

    public int CurrentIndex { get; private set; }

    [SerializeField] private UnityEngine.Events.UnityEvent<int> unlockk;
    [SerializeField] private UnityEngine.Events.UnityEvent<int> lockk;
    [SerializeField] private UnityEngine.Events.UnityEvent<bool> locked;

    public GameObject CurrentObject { 
        get { return ScreenList[CurrentIndex]; }
    }

    public void ChangeIndex(int newIndex) {
        CurrentIndex = newIndex;
        HandleUnlock();
    }

    private void HandleUnlock() {
        bool isUnlocked = CurrentIndex == unlockedIndex;
        if (locked != null) {
            locked.Invoke(isUnlocked);
        }
        if (unlockk != null && isUnlocked) {
            unlockk.Invoke(lockerIndex);
        }
        if (lockk != null && !isUnlocked) {
            lockk.Invoke(lockerIndex);
        }
    }

}
