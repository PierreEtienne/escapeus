using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightTrigger : AnimationTrigger
{
    [SerializeField] private string theValidClip;

    public void startAnimation(bool isError) {
        if (isError) {
            this.startAnimation();
        } else {
            TheAnimator.Play(TheValidClip);
        }
    }

    public string TheValidClip { get => theValidClip; }
}
