using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonStandController : MonoBehaviour
{
    [SerializeField] private int standIndex = 0;
    [SerializeField] private TorchController linkedTorch;
    [SerializeField] private bool shouldTrigger = false;
    // ===
    [SerializeField] private UnityEngine.Events.UnityEvent goNext;
    [SerializeField] private UnityEngine.Events.UnityEvent resetEverything;

    private Animator buttonAnimator;

    public int StandIndex {
        get => standIndex;
    }

    public bool ShouldTrigger {
        get => shouldTrigger;
        set => shouldTrigger = value;
    }

    void Start() {
        buttonAnimator = GetComponent<Animator>();
    }

    void Update() {
        /*
        if (shouldTrigger != buttonAnimator.GetBool("CanAnimate")) {
            buttonAnimator.SetBool("CanAnimate", shouldTrigger);
        }
        */
    }

    public void TriggerColor() {
        if (shouldTrigger && linkedTorch.TheTorchState != TorchController.TorchState.SUCCESS) {
            linkedTorch.TheTorchState = TorchController.TorchState.SUCCESS;
            goNext.Invoke();
        } else if (linkedTorch.TheTorchState != TorchController.TorchState.SUCCESS) {
            resetEverything.Invoke();
        }
    }

    public void SetError() {
        linkedTorch.TheTorchState = TorchController.TorchState.ERROR;
    }
}
