using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level3Controller : MonoBehaviour
{
    [SerializeField] private List<ButtonStandController> buttonStands;
    [Header("Animation - EndDoor")]
    [SerializeField] private Animator doorAnimator;
    [SerializeField] private string doorAnimationTrigger;
    [SerializeField] private UnityEngine.Events.UnityEvent doorOpen;

    private List<ButtonStandController> realButtonStands;
    private List<int> buttonIndexList;

    void Start()
    {
        realButtonStands = new List<ButtonStandController>();
        realButtonStands.Add(null);
        realButtonStands.Add(null);
        realButtonStands.Add(null);
        realButtonStands.Add(null);
        realButtonStands.Add(null);

        foreach (var item in buttonStands)
        {
            realButtonStands[item.StandIndex] = item;
        }

        SetupButtons();
        StartButton();
    }

    private void SetupButtons() {
        buttonIndexList = new List<int>();
        buttonIndexList.Add(-1);
        buttonIndexList.Add(-1);
        buttonIndexList.Add(-1);
        buttonIndexList.Add(-1);

        for (int i = 0; i < buttonIndexList.Count; i++) {
            int randomNumber = 0;
            bool alreadyPicked = true;
            
            while (alreadyPicked) {
                alreadyPicked = false;
                randomNumber = Random.Range(1, 5);

                foreach (var number in buttonIndexList) {
                    if (number == randomNumber) {
                        alreadyPicked = true;
                        break;
                    }
                }
            }
            
            buttonIndexList[i] = randomNumber;
        }

        string hiddenCode = "";
        hiddenCode += "" + buttonIndexList[0];
        hiddenCode += "" + buttonIndexList[1];
        hiddenCode += "" + buttonIndexList[2];
        hiddenCode += "" + buttonIndexList[3];

        Debug.Log(hiddenCode);
    }

    private void StartButton() {
        for (int i = 0; i < buttonIndexList.Count; i++) {
            realButtonStands[buttonIndexList[i]].ShouldTrigger = i == 0;
        }
    }
    
    public void GoNext() {
        bool setTrue = false;
        bool isEnd = false;

        for (int i = 1; i < buttonIndexList.Count; i++) {
            setTrue = !realButtonStands[buttonIndexList[i]].ShouldTrigger;
            if (setTrue) {
                realButtonStands[buttonIndexList[i]].ShouldTrigger = setTrue;
                break;    
            } else if (i == 3) {
                isEnd = true;
            }
        }

        if (isEnd) {
            doorAnimator.SetTrigger(doorAnimationTrigger);
            doorOpen.Invoke();
        }
    }

    public void ResetEverything() {
        for (int i = 0; i < buttonIndexList.Count; i++) {
            realButtonStands[buttonIndexList[i]].ShouldTrigger = i == 0;
            realButtonStands[buttonIndexList[i]].SetError();
        }
    }

    void Update()
    {
        
    }
}
