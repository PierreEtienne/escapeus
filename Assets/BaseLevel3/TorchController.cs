using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchController : MonoBehaviour
{
    [SerializeField] private List<GameObject> listSuccess;
    [SerializeField] private List<GameObject> listNormal;
    [SerializeField] private List<GameObject> listError;
    [SerializeField] private TorchState torchState = TorchState.NEW;
    
    private TorchState oldState = TorchState.NEW;

    public TorchState TheTorchState { get { return torchState; } set { torchState = value; } }

    void Start() {
        foreach (var item in listSuccess)
            item.SetActive(false);
        foreach (var item in listError)
            item.SetActive(false);
    }

    void Update() {
        if (oldState != torchState) {
            switch (oldState)
            {
                case TorchState.NEW:
                foreach (var item in listNormal)
                    item.SetActive(false);
                break;
                case TorchState.ERROR:
                foreach (var item in listError)
                    item.SetActive(false);
                break;
                case TorchState.SUCCESS:
                foreach (var item in listSuccess)
                    item.SetActive(false);
                break;
            }
            
            switch (torchState)
            {
                case TorchState.NEW:
                foreach (var item in listNormal)
                    item.SetActive(true);
                break;
                case TorchState.ERROR:
                foreach (var item in listError)
                    item.SetActive(true);
                break;
                case TorchState.SUCCESS:
                foreach (var item in listSuccess)
                    item.SetActive(true);
                break;
            }

            oldState = torchState;
        }
    }

    public enum TorchState {
        NEW = 1 << 1,
        ERROR = 1 << 2,
        SUCCESS = 1 << 3
    }
}
