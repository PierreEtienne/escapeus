using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
https://blog.studica.com/unity-tutorial-animator-controllers
https://www.youtube.com/watch?v=N73EWquTGSY
*/

public class WannaTest : MonoBehaviour
{
    [SerializeField]
    private GameObject wallObject;
    [SerializeField]
    private GameObject pillarObject;
    [SerializeField]
    private Animator animator;
    [SerializeField]
    private Animator myAnimator;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void movePositiveY() {
        Debug.Log("movePositiveY");
        Move(wallObject, 0.1f);
        Move(pillarObject, 0.1f);
    }

    public void moveNegativeY() {
        Debug.Log("moveNegativeY");
        Move(wallObject, -0.1f);
        Move(pillarObject, -0.1f);
    }

    public void moveReset() {
        Debug.Log("moveReset");
        Reset(wallObject);
        Reset(pillarObject);
    }

    public void animateCube() {
        animator.SetTrigger("MakeRed");
    }

    public void animateDoor() {
        myAnimator.SetTrigger("BringGame");
    }

    private void Move(GameObject gameObject, float amount) {
        gameObject.transform.position += Vector3.down * amount;
    }

    private void Reset(GameObject gameObject) {
        gameObject.transform.position = new Vector3(gameObject.transform.position.x, 0.0f, gameObject.transform.position.z);
    }
}
