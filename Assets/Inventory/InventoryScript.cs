using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryScript : MonoBehaviour
{
    [SerializeField] private List<String> nameList;
    [SerializeField] private List<GameObject> itemList;
    [SerializeField] private GameObject panelCase;

    // ---

    private const int MAX_CELL = 6;
    private const int CELL_HEIGHT = 200;

    // ---

    private IDictionary<int, string> activeCells = new SortedDictionary<int, string>();
    private IDictionary<int, GameObject> activeObjects = new SortedDictionary<int, GameObject>();

    // ---

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddObject(string v) {
        string normalizedName = v.ToLower();

        int index;
        for (index = 0; index < MAX_CELL; index++)
            if (!activeCells.ContainsKey(index))
                break;
        
        if (index == MAX_CELL) return;

        Vector3 position = new Vector3Int(0, index * -CELL_HEIGHT, 0);
        Transform parent = panelCase.transform;

        string newItem = "u-test";
        if(nameList.Contains(normalizedName)) {
            newItem = normalizedName;
        }

        GameObject newClone = Instantiate(itemList[nameList.IndexOf(newItem)], parent);
        newClone.name = string.Format("{0} - {1}", newItem, index);
        newClone.transform.localPosition += position;

        activeCells.Add(index, normalizedName);
        activeObjects.Add(index, newClone);
    }

    public void RemoveObject(string v) {
        string normalizedName = v.ToLower();
        
        if (!nameList.Contains(normalizedName)) return;
        if (!activeCells.Values.Contains(normalizedName)) return;

        KeyValuePair<int, string> entry;

        foreach (var x in activeCells)
            if (x.Value == normalizedName) {
                entry = x;
                break;
            }
        
        activeCells.Remove(entry);
        Destroy(activeObjects[entry.Key]);
        activeObjects.Remove(entry.Key);
    }
    
    public float GetTimeGain() {
        float timeGain = 0;

        foreach (var x in activeCells) {
            TimeGainScript timeGainScript = itemList[nameList.IndexOf(x.Value)].GetComponent<TimeGainScript>();
            timeGain += timeGainScript?.TimeGain ?? 0;
        }

        return timeGain;
    }

    public float GetTimeLoss() {
        float timeGain = 0;

        foreach (var x in activeCells) {
            TimeGainScript timeGainScript = itemList[nameList.IndexOf(x.Value)].GetComponent<TimeGainScript>();
            timeGain += timeGainScript?.TimeLoss ?? 0;
        }

        return timeGain;
    }
}
