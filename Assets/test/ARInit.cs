using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.Experimental.XR;

public class ARInit : MonoBehaviour
{
    public GameObject placementIndication;
    [SerializeField]
    private ARRaycastManager raycastManager;
    private Pose pose;
    private bool placementPoseIsValid = false;
    private bool isPlaced = false;
    [SerializeField]
    private ARSessionOrigin sessionOrigin;
    public GameObject placedObject;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        UpdatePlacement();
        UpdateIndicator();

        if (placementPoseIsValid && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began && !isPlaced) {
            isPlaced = true;
            placeObject();
        }
    }

    private void UpdateIndicator() {
        if (placementPoseIsValid) {
            placementIndication.SetActive(true);
            placementIndication.transform.SetPositionAndRotation(pose.position, pose.rotation);
        } else {
            placementIndication.SetActive(false);
        }
    }

    private void UpdatePlacement() {
        var screenCenter = sessionOrigin.camera.ViewportToScreenPoint(new Vector3(0.5f,0.5f));
        var hits = new List<ARRaycastHit>();
        raycastManager.Raycast(screenCenter, hits, TrackableType.Planes);
        placementPoseIsValid = hits.Count > 0;
        if (placementPoseIsValid) {
            pose = hits[0].pose;

            var camForward = sessionOrigin.camera.transform.forward;
            var camBearing = new Vector3(camForward.x,0,camForward.z).normalized;
            pose.rotation = Quaternion.LookRotation(camBearing);
        }
    }

    private void placeObject() {
        Instantiate(placedObject, pose.position, pose.rotation);
    }
}
